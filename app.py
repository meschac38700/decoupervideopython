from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
"""
src_video_path = input("Chemin du fichier à decouper (ex: ./videoSource.mp4): ")
target_path = input("Chemin de la future video (ex: ./destination.mp4): ")
"""
#if not src_video_path:
src_video_path = "./video1.mp4"
#if not src_video_path:
target_path = "./partie_2.mp4"
# ffmpeg_extract_subclip("full.mp4", start_seconds, end_seconds, targetname="cut.mp4")

t1 = 0
for i in range(0, 18):
    target_path = f"./parties_{i}.mp4"
    t2 = t1 + 3600
    ffmpeg_extract_subclip(src_video_path, t1, t2, targetname=target_path)
    t1 = t2